package com.example.usr1.myapplication;


import android.content.Intent;
import android.test.ActivityUnitTestCase;
import java.util.LinkedList;


/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ActivityUnitTestCase<MainActivity> {

    private MainActivity activity;

    LinkedList<GeoPoint> route ;


    public ApplicationTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        Intent intent = new Intent(getInstrumentation().getTargetContext(), MainActivity.class);
        startActivity(intent, null, null);
        activity = getActivity();
    }

    public void testBoxesNorthAmerica(){
        route = new LinkedList<GeoPoint>();
        route.add(new GeoPoint( 36.28   ,-103.82 ));
        route.add(new GeoPoint( 37.5  ,-102.5 ));
        route.add(new GeoPoint( 38.0  ,-101.7 ));
        route.add(new GeoPoint( 38.6  ,-100.0 ));
        Boxes boxes = Boxes.getBoxes(route);

        assertEquals( 38.6, boxes.mA.getLatitude());
        assertEquals( -103.82, boxes.mA.getLongitude() );

        assertEquals( 38.6, boxes.mB.getLatitude());
        assertEquals( -100.0, boxes.mB.getLongitude());

        assertEquals( 36.28 , boxes.mC.getLatitude());
        assertEquals( -100.0 , boxes.mC.getLongitude());

        assertEquals( 36.28, boxes.mD.getLatitude());
        assertEquals( -103.82 , boxes.mD.getLongitude());
    }

    public void testBoxesEurope(){
        route = new LinkedList<GeoPoint>();
        route.add(new GeoPoint( 45.46  ,3.7 ));
        route.add(new GeoPoint( 45.83 ,13.1 ));
        route.add(new GeoPoint( 45.03 ,19.44 ));
        route.add(new GeoPoint( 39.4  ,31.6 ));
        Boxes boxes = Boxes.getBoxes(route);

        assertEquals( 45.83, boxes.mA.getLatitude());
        assertEquals( 3.7, boxes.mA.getLongitude() );

        assertEquals( 45.83 , boxes.mB.getLatitude());
        assertEquals( 31.6 , boxes.mB.getLongitude());

        assertEquals( 39.4 , boxes.mC.getLatitude());
        assertEquals( 31.6 , boxes.mC.getLongitude());

        assertEquals( 39.4, boxes.mD.getLatitude());
        assertEquals( 3.7 , boxes.mD.getLongitude());
    }

    public void testBoxesSouthAmerica(){
        route = new LinkedList<GeoPoint>();
        route.add(new GeoPoint( -24.015584 ,-47.6490274 ));
        route.add(new GeoPoint( -23.011231 ,-47.1412412 ));
        route.add(new GeoPoint( -22.8669445 ,-47.1340865 ));
        route.add(new GeoPoint( -22.6711741 ,-47.3138865 ));
        route.add(new GeoPoint( -22.2152217 ,-47.3958372 ));
        route.add(new GeoPoint( -13.5063277  ,-47.1216836 ));
        Boxes boxes = Boxes.getBoxes(route);

        assertEquals( -13.5063277, boxes.mA.getLatitude());
        assertEquals( -47.6490274,boxes.mA.getLongitude() );

        assertEquals( -13.5063277, boxes.mB.getLatitude());
        assertEquals( -47.1216836, boxes.mB.getLongitude());

        assertEquals( -24.015584, boxes.mC.getLatitude());
        assertEquals( -47.1216836, boxes.mC.getLongitude());

        assertEquals( -24.015584, boxes.mD.getLatitude());
        assertEquals( -47.6490274, boxes.mD.getLongitude());
    }

    public void testBoxesCrossingIdl(){
        route = new LinkedList<GeoPoint>();
        route.add(new GeoPoint( 38.2593815, 126.0662135 ));
        route.add(new GeoPoint( 20.121141, -158.9578983 ));
        route.add(new GeoPoint( 31.7583989, 149.9510466 ));
        route.add(new GeoPoint( 14.0325279, -86.8262992 ));
        Boxes boxes = Boxes.getBoxes(route);

        assertEquals( 38.2593815, boxes.mA.getLatitude());
        assertEquals( 126.0662135  ,boxes.mA.getLongitude() );

        assertEquals( 38.2593815 , boxes.mB.getLatitude());
        assertEquals( -86.8262992  , boxes.mB.getLongitude());

        assertEquals( 14.0325279 , boxes.mC.getLatitude());
        assertEquals( -86.8262992 , boxes.mC.getLongitude());

        assertEquals( 14.0325279 , boxes.mD.getLatitude());
        assertEquals( 126.0662135 , boxes.mD.getLongitude());
    }

    public void testLaMoscow(){
        route = new LinkedList<GeoPoint>();
        route.add(new GeoPoint( 33.9429548, -118.4056869 ));
        route.add(new GeoPoint( 52.296172, -42.4538918 ));
        route.add(new GeoPoint( 52.3280501, -42.2039534 ));
        route.add(new GeoPoint( 55.2501577, -7.3339655 ));
        route.add(new GeoPoint( 55.7636869, 37.5634367 ));
        Boxes boxes = Boxes.getBoxes(route);

        assertEquals( 55.7636869, boxes.mA.getLatitude());
        assertEquals( -118.4056869  ,boxes.mA.getLongitude() );

        assertEquals( 55.7636869 , boxes.mB.getLatitude());
        assertEquals( 37.5634367  , boxes.mB.getLongitude());

        assertEquals( 33.9429548 , boxes.mC.getLatitude());
        assertEquals( 37.5634367 , boxes.mC.getLongitude());

        assertEquals( 33.9429548 , boxes.mD.getLatitude());
        assertEquals( -118.4056869 , boxes.mD.getLongitude());
    }

    public void testJapanBrasil(){
        route = new LinkedList<GeoPoint>();
        route.add(new GeoPoint( 35.2087561, 135.5864467 ));
        route.add(new GeoPoint( 7.6824258, 175.6187938 ));
        route.add(new GeoPoint( -7.8010649, -92.4616539 ));
        route.add(new GeoPoint( -13.6654941, -57.1305422 ));
        route.add(new GeoPoint( -25.767957, -46.0364989));
        Boxes boxes = Boxes.getBoxes(route);

        assertEquals( 35.2087561, boxes.mA.getLatitude());
        assertEquals( 135.5864467  ,boxes.mA.getLongitude() );

        assertEquals( 35.2087561 , boxes.mB.getLatitude());
        assertEquals( -46.0364989  , boxes.mB.getLongitude());

        assertEquals( -25.767957 , boxes.mC.getLatitude());
        assertEquals( -46.0364989 , boxes.mC.getLongitude());

        assertEquals( -25.767957 , boxes.mD.getLatitude());
        assertEquals( 135.5864467 , boxes.mD.getLongitude());

    }

    public void testBoxes(){
        route = new LinkedList<GeoPoint>();
        route.add(new GeoPoint( 52.3280501, -42.2039534 ));
        route.add(new GeoPoint( 55.2501577, -7.3339655 ));
        route.add(new GeoPoint( 35.2087561, 135.5864467 ));
        route.add(new GeoPoint( 14.0325279, -86.8262992 ));
        route.add(new GeoPoint( -23.011231 ,-47.1412412 ));
        route.add(new GeoPoint( 7.6824258, 175.6187938 ));
        route.add(new GeoPoint( -7.8010649, -92.4616539 ));
        route.add(new GeoPoint( -13.6654941, -57.1305422 ));
        route.add(new GeoPoint( 45.46  , 13.7 ));
        route.add(new GeoPoint( -32.36  ,213.7434 ));
        route.add(new GeoPoint( 7.6824258, 175.6187938 ));
        route.add(new GeoPoint( -7.8010649, -92.4616539 ));
        route.add(new GeoPoint( 38.2593815, 126.0662135 ));
        route.add(new GeoPoint( 20.121141, -158.9578983 ));
        route.add(new GeoPoint( 31.7583989, 149.9510466 ));
        Boxes boxes = Boxes.getBoxes(route);
        int lRep = 0;
        boolean FlagALongitude = false;
        boolean FlagALatitude = false;
        boolean FlagCLongitude = false;
        boolean FlagCLatitude = false;
        for (GeoPoint geoPoint : route){
            if (geoPoint.getLongitude() == boxes.mA.getLongitude()) {
                FlagALongitude = true;
            }
            if (geoPoint.getLongitude() == boxes.mC.getLongitude()) {
                FlagCLongitude = true;
            }
            if (geoPoint.getLatitude() == boxes.mA.getLatitude()) {
                FlagALatitude = true;
            }
            if (geoPoint.getLatitude() == boxes.mC.getLatitude()) {
                FlagCLatitude = true;
            }
        }
        assertTrue(FlagALatitude);
        assertTrue(FlagALongitude);
        assertTrue(FlagCLatitude);
        assertTrue(FlagCLongitude);
    }

    public void testIfInBoxes(){
        route = new LinkedList<GeoPoint>(); route.add(new GeoPoint( 52.3280501, -42.2039534 ));
        route.add(new GeoPoint( 52.3280501, -42.2039534 ));
        route.add(new GeoPoint( 55.2501577, -7.3339655 ));
        route.add(new GeoPoint( 35.2087561, 135.5864467 ));
        route.add(new GeoPoint( 14.0325279, -86.8262992 ));
        route.add(new GeoPoint( -23.011231 ,-47.1412412 ));
        route.add(new GeoPoint( 7.6824258, 175.6187938 ));
        route.add(new GeoPoint( -7.8010649, -92.4616539 ));
        route.add(new GeoPoint( -13.6654941, -57.1305422 ));
        route.add(new GeoPoint( 45.46  , 13.7 ));
        route.add(new GeoPoint( -32.36  ,213.7434 ));
        route.add(new GeoPoint( 7.6824258, 175.6187938 ));
        route.add(new GeoPoint( -7.8010649, -92.4616539 ));
        route.add(new GeoPoint( 38.2593815, 126.0662135 ));
        route.add(new GeoPoint( 20.121141, -158.9578983 ));
        route.add(new GeoPoint( 31.7583989, 149.9510466 ));
        Boxes boxes = Boxes.getBoxes(route);
        double lLongitude;
        double lLatitude;
        double ALongitude = boxes.mA.getLongitude();
        double CLongitude = boxes.mC.getLongitude();
        for (GeoPoint geoPoint : route){
            lLatitude = geoPoint.getLatitude();
            lLongitude = geoPoint.getLongitude();

            assertTrue(lLatitude <= boxes.mA.getLatitude());
            assertTrue(lLongitude >= ALongitude || ( (ALongitude - CLongitude ) > 0 && lLongitude <= CLongitude ) );
            assertTrue(lLatitude >= boxes.mC.getLatitude());
            assertTrue(lLongitude <= CLongitude || ( (ALongitude - CLongitude ) > 0 && lLongitude >= ALongitude ) );
        }
    }
}