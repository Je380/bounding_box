package com.example.usr1.myapplication;


import android.util.Log;

import java.util.LinkedList;

/**
 * Created by usr1 on 28/01/15.
 */
public class Boxes {
   public GeoPoint mA;
   public GeoPoint mB;
   public GeoPoint mC;
   public GeoPoint mD;

   public Boxes(GeoPoint a,GeoPoint b,GeoPoint c,GeoPoint d){
       mA = new GeoPoint(a);
       mB = new GeoPoint(b);
       mC = new GeoPoint(c);
       mD = new GeoPoint(d);
   }


   public static Boxes getBoxes(LinkedList<GeoPoint> route){
       double lMaxLatitude = -90;
       double lMinLatitude = 90;
       double lMaxLongitude = -180;
       double lMinLongitude = 180;

       for(GeoPoint geoPoint : route){
           //Log.d("getPoint",geoPoint.getLatitude()+"     "+geoPoint.getLongitude());
           if (lMaxLatitude < geoPoint.getLatitude()){
               lMaxLatitude = geoPoint.getLatitude();
           }
           if (lMinLatitude > geoPoint.getLatitude()){
               lMinLatitude = geoPoint.getLatitude();
           }
           if (lMaxLongitude < geoPoint.getLongitude()){
               lMaxLongitude = geoPoint.getLongitude();
           }
           if (lMinLongitude > geoPoint.getLongitude()){
               lMinLongitude = geoPoint.getLongitude();
           }
       }
       //Log.d("getPointMax",lMaxLatitude+"     "+lMaxLongitude);
       //Log.d("getPointMin",lMinLatitude+"     "+lMinLongitude);
       double lDifference = Math.abs(lMaxLongitude - lMinLongitude);

       if ( (lDifference > 180 ) && lMaxLongitude > 0 && lMinLongitude < 0 ) {
            double lALongitude = 180;
            double lBLongitude = -180;
            for (GeoPoint geoPoint : route){
               double lLongitude = geoPoint.getLongitude();
               if ( lLongitude >= 0 && lALongitude > lLongitude){
                   lALongitude = lLongitude;
               }
               if ( lLongitude <= 0 && lBLongitude < lLongitude){
                   lBLongitude = lLongitude;
               }
            }
           return new Boxes(new GeoPoint(lMaxLatitude, lALongitude),
                            new GeoPoint(lMaxLatitude, lBLongitude),
                            new GeoPoint(lMinLatitude, lBLongitude),
                            new GeoPoint(lMinLatitude, lALongitude));
       }
       return new Boxes(new GeoPoint(lMaxLatitude, lMinLongitude),
                        new GeoPoint(lMaxLatitude, lMaxLongitude),
                        new GeoPoint(lMinLatitude, lMaxLongitude),
                        new GeoPoint(lMinLatitude, lMinLongitude));
   }
}
