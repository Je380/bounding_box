package com.example.usr1.myapplication;

/**
 * Created by usr1 on 29/01/15.
 */
public class GeoPoint {
    private final double mLatitude;
    private final double mLongitude;

    public GeoPoint(double i,double i2){
        mLatitude = i;
        mLongitude = i2;
    }

    public GeoPoint(GeoPoint obj){
        mLatitude = obj.getLatitude();
        mLongitude = obj.getLongitude();
    }

    public double getLatitude(){
     return mLatitude;
    }

    public double getLongitude(){
     return mLongitude;
    }
}
