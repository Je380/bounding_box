package com.example.usr1.myapplication;


import android.app.Activity;
import android.os.Bundle;

import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.LinkedList;


public class MainActivity extends Activity{

    private EditText mParam1;
    private EditText mParam2;
    private TextView mResult;
    private Button mButton;

    LinkedList<GeoPoint> route ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mParam1 = (EditText) findViewById(R.id.par1);
        mParam2 = (EditText) findViewById(R.id.par2);
        mResult = (TextView) findViewById(R.id.result);
        mButton = (Button) findViewById(R.id.button);

        route = new LinkedList<GeoPoint>();

        route.add(new GeoPoint( 85.5232182 ,-178.890351 ));
        route.add(new GeoPoint( 83.5453182 ,-148.890351 ));
        route.add(new GeoPoint( 83.5453182 ,-98.890351 ));
        route.add(new GeoPoint( 83.5453182 ,-88.890351 ));
        route.add(new GeoPoint( 83.5453182 ,-48.890351 ));
        route.add(new GeoPoint( 83.5453182 ,-18.890351 ));
        route.add(new GeoPoint( 83.5453182 ,18.890351 ));
        route.add(new GeoPoint( 78.211791 ,22.0446717 ));
        route.add(new GeoPoint( 83.5453182 ,44.890351 ));
        route.add(new GeoPoint( 83.5453182 , 98.890351 ));
        route.add(new GeoPoint( 83.5453182 ,118.890351 ));
        route.add(new GeoPoint( 83.5453182 ,148.890351 ));
        route.add(new GeoPoint( 84.064307, 162.453523));
        route.add(new GeoPoint( 84.064307, -177.453523));

        Boxes boxes = Boxes.getBoxes(route);
        Log.e("Boxes A Latitude",boxes.mA.getLatitude()+"");
        Log.e("Boxes A Longitude",boxes.mA.getLongitude()+"");

        Log.e("Boxes B Latitude",boxes.mB.getLatitude()+"");
        Log.e("Boxes B Longitude",boxes.mB.getLongitude()+"");

        Log.e("Boxes C Latitude",boxes.mC.getLatitude()+"");
        Log.e("Boxes C Longitude",boxes.mC.getLongitude()+"");

        Log.e("Boxes D Latitude",boxes.mD.getLatitude()+"");
        Log.e("Boxes D Longitude",boxes.mD.getLongitude()+"");

    }
}
